/*
 * Desc: Blog Page - renders a specific blog
 */
import React from "react";
import markdown from "markdown";

const defaultProps = {
  id: "12345",
  slug: "",
  title: "dummy blog title",
  content: "hi there ssup?"
};

const Blog = (props = defaultProps) => {
  const blog = props || {}; // todo: to be change to props
  console.log('=======>', props);
  const parsedHTMLContent = markdown.parse(blog.content || '');
  const parsedHTMLTitle = markdown.parse(blog.title || '');
  return (
    <div>
      <section className="blog-title">{parsedHTMLTitle}</section>
      <section className="blog-content">{parsedHTMLContent}</section>
    </div>
  );
};

export default Blog;
