import React, { useEffect, useState } from "react";
import { fetchBlog } from "../../services/blogService";
import Blog from './Blog';

const defaultProps = {
  params: {
    // id: "12345",
    // slug: "",
    // title: "dummy blog title",
    // content: ""
  }
};

const BlogContainer = (props) => {
  const { id } = defaultProps.params; // todo replaced by props
  const [blog, setBlog] = useState({});
  useEffect(() => {
    fetchBlog(id).then(res => {
      console.log('======> res', res);
      if (res.success) {
        setBlog(res.blog);
      }
    });
  }, [id]);
  console.log('=========> blog', blog);
  return (
    <Blog {...blog} />
  )
};

export default BlogContainer;
