/*
* Desc: contains the routes of the UI components
*/
import React, { Component } from 'react';
import { Router, Link } from "@reach/router"
import Blog from './components/blog';

class App extends Component {
  render() {
    return (
      // <Blog>chk</Blog>
      <Router>
        <Blog path='/' />
      </Router>
    )
  }
}

export default App