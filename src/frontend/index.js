/*
* Desc: entry point of frontend application
*/
import React from "react";
import App from './routes';
import ReactDOM from 'react-dom';

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
