import axios from "axios";

const BASE_URL = "localhost:8000";

export const fetchBlog = () => {
  return new Promise(res => (res({
    blog: {
      id: '1234',
      content: 'hi there',
    },
    success: true,
  })));
};

// export const fetchBlog = blogId =>
//   axios
//     .get(`${BASE_URL}/blogs/${blogId}`)
//     .then(res => {
//       const { blog } = res.data;
//       return { blog, success: true };
//     })
//     .catch(error => ({ success: false, error }));
